library(data.table)

## TODO: Create function to add utr5.index like reindexUtr3 does.
reindexUtr3 <- function(gcache, flank.up=1000L, flank.down=flank.up,
                        stranded=TRUE, gene.by='all', gene.collapse='cover',
                        gene.cds.cover='min', chrs=NULL, verbose=FALSE,
                        path=NULL, return.anno=TRUE) {
  if (is.null(chrs)) {
    chrs <- chromosomes(gcache)
  }
  if (is.null(path)) {
    path <- file.path(cacheDir(gcache), 'annotated.chromosomes')
  }
  files <- sapply(chrs, function(chr) {
    f <- GenomicCache:::annotatedChromosomeFN(gcache, chr,
                                             gene.collapse=gene.collapse,
                                             cds.cover=gene.cds.cover,
                                             flank.up=flank.up,
                                             flank.down=flank.down,
                                             stranded=stranded)
    if (!file.exists(f)) {
      cat(f, "does no exist, skipping\n.")
      NULL
    } else {
      f
    }
  })
  files <- files[!sapply(files, is.null)]

  # ret <- foreach(file=files, .packages=c("GenomicCache", 'data.table'),
  #                .inorder=FALSE, .verbose=verbose) %dopar% {
  ret <- mclapply(files, function(file) {
    .chr <- strsplit(basename(file), '.', fixed=TRUE)[[1]][1]
    cat(.chr, "...\n")
    if (.chr %in% chrs) {
      annotated <- load.it(file)
      annotated <- indexUtr3(annotated)
      save(annotated, file=file)
    }
    if (return.anno) annotated else file
  }, mc.preschedule=FALSE)

  if (return.anno) {
    ret <- do.call(c, unname(ret))
    ret <- ret[order(ret)]
  }

  ret
}

##' Annotate 3'UTRs by adding a "utr3.index" column to the annotated chromosome.
##'
##' 3'UTRs share the same index if they aren't separated by a cds from any of
##' the isoforms in the gene. The most distal/last 3'utr is indexed with the
##' highest number.
##'
##' @param annotated.chr An \code{\linkS4class{AnnotatedChromosome}} object
##' @return The same \code{\linkS4class{AnnotatedChromosome}} object with an
##' added \code{utr3.index} column.
indexUtr3 <- function(annotated.chr) {
  dt.anno <- as(annotated.chr, 'data.table')
  setkeyv(dt.anno, c('entrez.id', 'exon.anno'))
  iter <- unique(as.character(dt.anno$entrez.id))
  iter <- iter[!is.na(iter)]
  splits <- lapply(iter, function(sub) {
    ## cat(sub, "\n")
    if (is.na(sub)) {
      return(NULL)
    }
    ## cat(sub, "...\n")
    anno <- dt.anno[J(sub), mult='all']      # all annos for this gene
    anno.u3 <- anno[exon.anno %like% "utr3"] # includes utr3 and utr3*
    ## anno.u3 <- anno[J(sub, 'utr3'), mult='all']

    anno.u3.ir <- as(anno.u3, 'IRanges')
    if (length(anno.u3.ir) == 0L) {
      return(NULL)
    }
    values(anno.u3.ir) <- NULL

    u3.bounds <- range(anno.u3.ir)

    ## anno.cds will be used to "split" the 3'utr annotation so we can determine
    ## the index of the exons.
    anno.cds <- anno[J(sub, 'cds'), mult='all']
    if (nrow(anno.cds) == 1L && is.na(anno.cds$start)) {
      ## No CDS annotation -- do something about this
      ## This must be a non-translated molecule -- so no cds annotation, use
      ## 'utr'
      anno.cds <- anno[J(sub, 'utr'), mult='all']
      if (nrow(anno.cds) == 1L && is.na(anno.cds$start)) {
        return(NULL)
      }
    }
    anno.cds <- as(anno.cds, 'IRanges')
    values(anno.cds) <- NULL ## setops with IRanges+elementMetadata fail

    u3 <- subsetByOverlaps(setdiff(u3.bounds, anno.cds), anno.u3.ir)

    ## This is why we have to do a subsetByOverlaps(setdiff(...))
    ## --- : utr3
    ## *** : cds
    ##
    ## ----  -----  *****   ***** ----
    ##
    ## -------------------------------  u3.bounds
    ## -------------*****---*****-----  setdiff(u3.bounds, anno.cds)
    ##                   ^^^:need to remove to get correct numbering of utr3
    ## ----  -----                ----
    ##  1      1                   2
    o <- findOverlaps(anno.u3.ir, u3)
    sh <- subjectHits(o)
    qh <- queryHits(o)

    if (anno$strand[1] == '-') {
      ## 1-3 is from most proximal to most distal (no matter what strand)
      sh <- seq(max(sh), 1)[sh]
    }

    ## u <- GRanges(seqnames=anno.u3$seqnames, ranges=anno.u3.ir,
    ##              strand=anno.u3$strand)
    ## values(u) <- DataFrame(utr3.index=sh, symbol=anno.u3$symbol[1],
    ##                        entrez.id=anno.u3$entrez.id[1])
    ## values(u) <- DataFrame(utr3.index=sh)
    GRanges(seqnames=anno.u3$seqnames, ranges=anno.u3.ir, strand=anno.u3$strand,
            utr3.index=sh)

  })
  splits <- splits[!sapply(splits, is.null)]
  splits <- do.call(c, splits)

  values(annotated.chr)$utr3.index <- 0L
  o <- findOverlaps(annotated.chr, splits)
  qh <- queryHits(o)
  sh <- subjectHits(o)
  values(annotated.chr[qh])$utr3.index <- values(splits[sh])$utr3.index
  annotated.chr
}

##' the maximum utr3 index is the most distal 3'utr
##'
##' @param ag AnnotatedGenome object
##' @param si.object An object with seqinfo that you want ag to mimic
annotateIntronUtr3 <- function(ag, si.object) {
  # stop("Moved to GenomicCache")
  if (inherits(try(seqinfo(si.object)), "try-error")) {
    stop("An object with `seqinfo` is required for `si.object`")
  }
  if (!is.numeric(values(ag)$utr3.index)) {
    stop("missing `utr3.index` column, run `indexUtr3`")
  }
  agdt <- as(ag, 'data.frame')
  levels(agdt$exon.anno) <- c(levels(agdt$exon.anno), 'intron.utr3')

  agdt <- data.table(agdt, key=c('seqnames', 'strand', 'entrez.id', 'start'))

  re <- agdt[, {
    sd <- copy(.SD)
    is.iutr3 <- utr3.index > 0L & utr3.index < max(utr3.index)
    sd$exon.anno[is.iutr3] <- 'intron.utr3'
    sd
  }, by= eval( head(key(agdt), -1L) ) ]

  gr <- rematchSeqinfo(as(re, 'GRanges'), si.object)
  gr <- gr[order(gr)]

  for (name in names(values(gr))) {
    if (is.factor(values(gr)[[name]])) {
      values(gr)[[name]] <- as.character(values(gr)[[name]])
    }
  }

  gr
}

## Testing correct labeling of utr3 chunks
## anno.u3 <- IRanges(c(10, 20, 50), width=5)
## anno.cds <- IRanges(c(30, 40), width=5)
## u3.bounds <- range(anno.u3)
## u3 <- subsetByOverlaps(setdiff(u3.bounds, anno.cds), anno.u3)
## sh <- subjectHits(findOverlaps(anno.u3, u3))
